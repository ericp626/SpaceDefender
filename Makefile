bin/SpaceDefender: bin obj/main.o obj/system.o obj/animated_spritesheet.o obj/spritesheet.o obj/sprite.o
	gcc -o bin/SpaceDefender obj/main.o obj/system.o obj/animated_spritesheet.o obj/spritesheet.o obj/sprite.o -lSDL2 -lSDL2_image -lSDL2_ttf
	cp bin/SpaceDefender .

obj/system.o: obj src/system.c src/system.h
	gcc -c src/system.c -o obj/system.o

obj/animated_spritesheet.o: obj src/animated_spritesheet.c src/animated_spritesheet.h
	gcc -c src/animated_spritesheet.c -o obj/animated_spritesheet.o

obj/spritesheet.o: obj src/spritesheet.c src/spritesheet.h
	gcc -c src/spritesheet.c -o obj/spritesheet.o

obj/sprite.o: obj src/sprite.c src/sprite.h
	gcc -c src/sprite.c -o obj/sprite.o

obj/main.o: obj src/main.c
	gcc -c src/main.c -o obj/main.o

obj:
	mkdir obj

bin:
	mkdir bin

.PHONY: clean

clean:
	rm -rf bin
	rm -rf obj
	rm SpaceDefender

