# Space Defender

This is a simple demo game in order to test SDL. Not sure if it will eventually end up as a real game.For now it's just a start menu. I was originaly thinking of doing a sort of space invader clone.

This demo is has been made after reading the tutorial code from

[TwinklebearDev SDL 2.0 Tutorial Index](http://www.willusher.io/pages/sdl2/)

If you use my code, just don't forget to cite the reference. I will myself try to use the Spritesheet and Animation code from this project

[maze-sdl](https://bitbucket.org/ablondin-projects/maze-sdl/overview)

## Requirements

* SDL2
* SDL2_image
* SDL2_ttf

## Building the project

You can build the project by typing 

~~~
make
~~~

It will copy the executable file in the root to make sure it can access the assets folder. Then you can type:

~~~
./SpaceDefender
~~~

to run the program.

## Playing the game

There is no game yet. You can simply move the menu up or down with the arrows and press enter to select the menu. The only valid menu so far is quit.


