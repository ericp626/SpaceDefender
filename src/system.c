/**
 * System specific methods
 */

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>
#include "system.h"

#define MENU_CURSOR_ANIMATION_DELAY    187 // 3/16 of seconds

void clear_resource_pointers ( sResources *res )
{
   res->win = NULL;
   res->ren = NULL;
   res->tex_title = NULL;
   res->texmsg_start = NULL;
   res->texmsg_quit = NULL;
   res->spr_arrow = NULL;
   res->fnt = NULL;

}

// --------------
// Public Methods
// --------------

bool initialise_SDL()
{
   if ( SDL_Init (SDL_INIT_VIDEO) != 0)
   {
      printf ("SDL_Init Error: %s\n", SDL_GetError() );
      return false;
   }

   if ( IMG_Init ( IMG_INIT_PNG ) == 0)
   {
      printf ( "IMG_Init Error: loading the PNG image processing\n");
      return false;
   }

   if ( TTF_Init () != 0 )
   {
      printf ( "TTF_Init Error: Cannot load the TTF module\n");
      return false;
   }

   return true;
}

void close_SDL ()
{
   TTF_Quit();
   IMG_Quit();
   SDL_Quit();
}

bool load_resources ( sResources *res)
{
   // temporary surfaces to build textures
   SDL_Surface *bmp_title;
   SDL_Surface *bmpmsg_start;
   SDL_Surface *bmpmsg_quit;
//   SDL_Surface *bmp_arrow;

   SDL_Color fnt_color = { .a= 255, .r=200, .g=200, .b=255 };

   clear_resource_pointers( res );

   res->win = SDL_CreateWindow ("Space Defender", 100, 100,
         SCREEN_WIDTH, SCREEN_HEIGHT, SDL_WINDOW_SHOWN);

   if ( res->win == NULL)
   {
      printf ("Error: Cannot create window\n");
      return false;
   }

   res->ren = SDL_CreateRenderer
            (res->win, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
   if ( res->ren == NULL )
   {
      printf ("Error: Cannot create renderer\n");
      return false;
   }

   bmp_title = IMG_Load("assets/title.png");
   if ( bmp_title == NULL)
   {
      printf ("Error: Cannot load Title Bitmap\n");
      return false;
   }

   res->tex_title = SDL_CreateTextureFromSurface (res->ren, bmp_title);
   SDL_FreeSurface( bmp_title );
   if ( res->tex_title == NULL )
   {
      printf ("Error: Cannot create Title texture\n");
      return false;
   }

   res->fnt = TTF_OpenFont("assets/future.ttf", MENU_FONT_POINTSIZE);
   if ( res->fnt == NULL)
   {
      printf ( "Error: Cannot load True Type Font\n");
      return false;
   }

   bmpmsg_start = TTF_RenderText_Blended(res->fnt, "Start Game", fnt_color );
   bmpmsg_quit = TTF_RenderText_Blended(res->fnt, "Quit", fnt_color);
   if ( bmpmsg_start == NULL && bmpmsg_quit == NULL )
   {
      printf ( "Error: Cannot create Menu Messages\n" );
      return false;
   }

   res->texmsg_start = SDL_CreateTextureFromSurface ( res->ren, bmpmsg_start );
   res->texmsg_quit = SDL_CreateTextureFromSurface ( res->ren, bmpmsg_quit );
   SDL_FreeSurface ( bmpmsg_start );
   SDL_FreeSurface ( bmpmsg_quit );
   if ( res->texmsg_start == NULL && res->texmsg_quit == NULL )
   {
      printf ("Error: Cannot create menu textures\n ");
      return false;
   }

   res->spr_arrow = sprite_create( "assets/arrow.png", 1, 16,
      MENU_CURSOR_ANIMATION_DELAY, res->ren );

   /*bmp_arrow = IMG_Load("assets/arrow.png");
   if ( bmp_title == NULL)
   {
      printf ("Error: Cannot load Arrow Bitmap\n");
      return false;
   }

   res->tex_arrow = SDL_CreateTextureFromSurface (res->ren, bmp_arrow);
   SDL_FreeSurface( bmp_arrow );
   if ( res->tex_arrow == NULL )
   {
      printf ("Error: Cannot create Arrow texture\n");
      return false;
   }*/


   return true;
}

void unload_resources ( sResources *res )
{
   if ( res->win != NULL )          SDL_DestroyWindow(res->win);
   if ( res->ren != NULL )          SDL_DestroyRenderer(res->ren);
   if ( res->tex_title != NULL )    SDL_DestroyTexture(res->tex_title);
   if ( res->fnt != NULL )          TTF_CloseFont ( res->fnt );
   if ( res->texmsg_start != NULL ) SDL_DestroyTexture (res->texmsg_start);
   if ( res->texmsg_quit != NULL )  SDL_DestroyTexture (res->texmsg_quit);
   if ( res->spr_arrow != NULL )    sprite_delete (res->spr_arrow);
   clear_resource_pointers( res );
}
