
#include "math.h"
#include "sprite.h"

sSprite *sprite_create(const char *filename, int numRows, int numColumns,
                              int delayBetweenFrame, SDL_Renderer* renderer)
{
   sSprite *s;
   s = (sSprite*) malloc ( sizeof ( sSprite));
   s->sheet = AnimatedSpritesheet_create( filename, numRows, numColumns,
      numRows * numColumns, delayBetweenFrame, renderer );
   s->position.x = 0;
   s->position.y = 0;
   s->destination.x = 0;
   s->destination.y = 0;
   s->speed = 100; // set a default speed to avoid division by 0 in render
   s->last_update = SDL_GetTicks();
   return s;

}

void sprite_delete(sSprite *sprite)
{
   if ( sprite != NULL)
   {
      AnimatedSpritesheet_delete( sprite->sheet );
      free ( sprite );
   }
}

void sprite_render (sSprite *sprite)
{
   int elapsed = SDL_GetTicks() - sprite->last_update;
   int ticks_per_pixel = ( 1000 / sprite->speed );
   //printf ("DEBUG: Thicks per pixel are %d for speed %d\n", ticks_per_pixel, sprite->speed );
   int nb_pixel = elapsed / ticks_per_pixel;
   //printf ("DEBUG: Nb pixel is %d for elapsed time %d\n", nb_pixel, elapsed);
   sprite->last_update += nb_pixel * ticks_per_pixel;
   int dif_x = sprite->position.x - sprite->destination.x;
   int dif_y = sprite->position.y - sprite->destination.y;

   // update the position of the sprite

   if ( dif_x != 0 )
   {
      if ( abs(dif_x) < nb_pixel ) nb_pixel = abs(dif_x);
      if ( dif_x < 0 ) sprite->position.x += nb_pixel;
      else sprite->position.x -= nb_pixel;
   }

   if ( dif_y != 0 )
   {
      if ( abs(dif_y) < nb_pixel ) nb_pixel = abs(dif_y);
      if ( dif_y < 0 ) sprite->position.y += nb_pixel;
      else sprite->position.y -= nb_pixel;
   }

   AnimatedSpritesheet_render( sprite->sheet, sprite->position.x, sprite->position.y);
}

void sprite_moveto (sSprite *sprite, int dst_x, int dst_y , int speed )
{
   sprite->destination.x = dst_x;
   sprite->destination.y = dst_y;
   sprite->speed = speed;
   if ( sprite->speed > 1000) sprite->speed = 1000;
   //printf ("DEBUG: Moving to ( %d, %d)\n", dst_x, dst_y);
}

void sprite_set_position ( sSprite *sprite, int x, int y )
{
   sprite->position.x = x;
   sprite->position.y = y;
   sprite->destination.x = x;
   sprite->destination.y = y;
}

void sprite_animation_start ( sSprite *sprite )
{
   AnimatedSpritesheet_run( sprite->sheet );
}

void sprite_animation_stop (sSprite *sprite )
{
   AnimatedSpritesheet_stop( sprite->sheet );
}

void sprite_animation_change ( sSprite *sprite, int row )
{
   AnimatedSpritesheet_setRow( sprite->sheet, row );
}
