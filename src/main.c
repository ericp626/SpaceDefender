/**
 * Test program to try SDL. Do an initial testing of the SDL library.
 * Creates only an animated title screen
 *
 * The code has been built accordint to this tutorial
 * http://www.willusher.io/pages/sdl2/
 *
 * @author: Eric Pietrocupo
 * @date: Novembre 21st, 2017
 *
 * License GNU GPL
 */

#include <stdio.h>
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <stdbool.h>
#include <SDL2/SDL_ttf.h>
#include "system.h"

#define MENU_CURSOR_SPEED  64

void title_mainloop ( sResources res )
{
   SDL_Rect pos_title;
   SDL_Rect pos_menu_item;
   SDL_Rect pos_menu_anchor;
   SDL_Event evn;
   bool quit = false;
   int menu_cursor = 0;
   int menu_x_position = 0;

   // precalculate image position of title and menu to save time
   SDL_QueryTexture( res.tex_title, NULL, NULL, &pos_title.w, &pos_title.h );
   pos_title.x = (SCREEN_WIDTH / 2) - (pos_title.w / 2);
   pos_title.y = (SCREEN_HEIGHT / 4) - ( pos_title.h / 2 );

   SDL_QueryTexture( res.texmsg_start, NULL, NULL, &pos_menu_anchor.w, &pos_menu_anchor.h );
   pos_menu_anchor.x = (SCREEN_WIDTH / 2) - ( pos_menu_anchor.w /2 );
   pos_menu_anchor.y = ((SCREEN_HEIGHT / 4) * 3 ) - pos_menu_anchor.h;
   //NOTE: the first menu item is the largest item in the menu, so we use it

   sprite_animation_start( res.spr_arrow );
   menu_x_position = pos_menu_anchor.x -
    (res.spr_arrow->sheet->spritesheet->spriteWidth + 8 );
   sprite_set_position( res.spr_arrow, menu_x_position, pos_menu_anchor.y );


   while ( quit == false )
   {
      // ---------- Read input ----------

      while (SDL_PollEvent(&evn) == 1)
      {
         if (evn.type == SDL_QUIT) quit = true;
         if (evn.type == SDL_KEYDOWN )
         {
            if ( evn.key.keysym.scancode == SDL_SCANCODE_ESCAPE ) quit = true;
            else if ( evn.key.keysym.scancode == SDL_SCANCODE_UP )
            {
               if (menu_cursor > 0 )
               {
                  menu_cursor--;
                  sprite_moveto( res.spr_arrow, menu_x_position,
                     pos_menu_anchor.y + (pos_menu_anchor.h * menu_cursor),
                     MENU_CURSOR_SPEED  );
                  //printf ("DEBUG: Menu up: Cursor = %d\n", menu_cursor);
               }
            }
            else if ( evn.key.keysym.scancode == SDL_SCANCODE_DOWN )
            {
               if (menu_cursor < 1 )
               {
                  menu_cursor++;
                  sprite_moveto( res.spr_arrow, menu_x_position,
                     pos_menu_anchor.y + (pos_menu_anchor.h * menu_cursor),
                     MENU_CURSOR_SPEED );
                  //printf ("DEBUG: Menu down: Cursor = %d\n", menu_cursor);
               }
            }
            else if ( evn.key.keysym.scancode == SDL_SCANCODE_RETURN )
            {
               if ( menu_cursor == 1 ) quit = true;
            }
            //NOTE: I am not using a switch statement to optimise speed
         }
		}

      // ---------- Render ----------
      // NOTE: The update of the position and animation are made inside the render
      // functions of sprite.

      SDL_RenderClear ( res.ren );

      SDL_RenderCopy ( res.ren, res.tex_title, NULL, &pos_title );

      SDL_QueryTexture( res.texmsg_start, NULL, NULL, &pos_menu_item.w, &pos_menu_item.h );
      pos_menu_item.x = pos_menu_anchor.x;
      pos_menu_item.y = pos_menu_anchor.y;
      SDL_RenderCopy ( res.ren, res.texmsg_start, NULL, &pos_menu_item );
      SDL_QueryTexture( res.texmsg_quit, NULL, NULL, &pos_menu_item.w, &pos_menu_item.h );
      pos_menu_item.y += pos_menu_item.h;
      SDL_RenderCopy ( res.ren, res.texmsg_quit, NULL, &pos_menu_item );

      sprite_render( res.spr_arrow );
      SDL_RenderPresent ( res.ren );
   }


}

int main ()
{
   sResources res;

   if ( initialise_SDL() == true )
   {
      if ( load_resources(&res) == true)
      {
         title_mainloop( res );
      }
      unload_resources( &res);
      close_SDL();
   }


}
