/**
* Encapsulate the animated sprite sheet to add movement information used
* by normally all sprite. It's basically destination, velocity and timing
* information.
*
* @author: Eric Pietrocupo
* @date: nov 26th, 2017
*/
#ifndef SPRITE_H_INCLUDED
#define SPRITE_H_INCLUDED

#include <SDL2/SDL.h>
#include "animated_spritesheet.h"

// Structure Definition

typedef struct
{
   int x;
   int y;
}
sVector2;

typedef struct
{
   struct AnimatedSpritesheet  *sheet;
   sVector2             position;
   sVector2             destination;
   int                  speed;
   int                  last_update;
}sSprite;
//NOTE: last_update will hold complete pixel movement to avoid keeping into
// a second variable a remainder to time which was not used yet for movement.

/*
* Creates a sprite dynamically and return a pointer to the structure. Most
* parameters are used for the animated spritesheet since all animation
* information are set to 0 at creation.
*
* @param filename          file containing the picture
* @param numRows           number of rows in the sheet
* @param numColumns        number of columns in the sheet
* @param delayBetweenFrame number of miliseconds before changing animation
* @param renderer          renderer that will be used for drawing the sprite.
*/
sSprite *sprite_create(const char *filename, int numRows, int numColumns,
                              int delayBetweenFrame, SDL_Renderer* renderer);

/*
* Sprite to delete and free the memory.
*
* @param sprite sprite to change
*/
void sprite_delete(sSprite *sprite);

/*
* Update the position and render the sprite on the screen. The method will
* query time by itself to perform movement.
*
* @param sprite sprite to change
*/
void sprite_render (sSprite *sprite);

/*
* Update the position and render the sprite on the screen. The method will
* query time by itself to perform movement.
*
* NOTE: speed is applied equally to the X and Y axis, which means that moving
* in diagonal will be faster.
*
* @param sprite  sprite to change
* @param dst_x   destination X to move to
* @param dst_y   destination Y to move to
* @param speed   Movement speed in pixels per seconds (max=1000)
*/
void sprite_moveto (sSprite *sprite, int dst_x, int dst_y , int speed );

/*
* Set an initial position to the object to avoid movement to that destination.
* Could alsp be used to teleport a sprite.
*
* @param sprite sprite to change
* @param x      sprite X position
* @param y      sprite Y position
*/
void sprite_set_position ( sSprite *sprite, int x, int y );

/*
* Start the animation sequence
*
* @param sprite sprite to change
*/
void sprite_animation_start ( sSprite *sprite );

/*
* Stop the animation sequence
*
* @param sprite sprite to change
*/
void sprite_animation_stop (sSprite *sprite );

/*
* Change the animation sequence
*
* @param sprite sprite to change
*/
void sprite_animation_change ( sSprite *sprite, int row );



#endif // SPRITE_H_INCLUDED
