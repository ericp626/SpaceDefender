/**
 * System specific methods
 */

#ifndef SYSTEM_H_INCLUDED
#define SYSTEM_H_INCLUDED

#include "sprite.h"
#include <stdbool.h>

#define SCREEN_WIDTH 640
#define SCREEN_HEIGHT 480
#define MENU_FONT_POINTSIZE 64

typedef struct
{
   SDL_Window *win;
   SDL_Renderer *ren;
   SDL_Texture *tex_title;
   SDL_Texture *texmsg_start;
   SDL_Texture *texmsg_quit;
   sSprite *spr_arrow;
   TTF_Font *fnt;
}
sResources;

/**
 * Initialise the library
 * @return false if the initialisation failed
 */
bool initialise_SDL ();

/**
 * Close the library
 */
void close_SDL ();

/**
 * Load the necessary resources
 * @param res structure that will contain the loaded resources
 * @return    false if the resources failled to be created
 */
bool load_resources ( sResources *res);

/*
 * Free the memory of the loaded resources
 * @param res Structure of resource to free from the memory
 */
void unload_resources ( sResources *res);

#endif // SYSTEM_H_INCLUDED
